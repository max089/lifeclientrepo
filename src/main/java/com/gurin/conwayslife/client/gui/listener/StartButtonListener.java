package com.gurin.conwayslife.client.gui.listener;

import java.awt.Component;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.gurin.conwayslife.client.gui.FieldDrawConstants;
import com.gurin.conwayslife.client.gui.GameFieldPanel;
import com.gurin.conwayslife.client.model.setting.ConnectionSettings;
import com.gurin.conwayslife.client.model.setting.ConnectionSettingsException;
import com.gurin.conwayslife.client.model.thread.GameFieldRefresher;

/*
 * ��� ���� ������� ���������
 * 1. ������������ ������� ������
 * 2. ������������ �������� ������
 * 3. �������� ����
 * 4. ��������� �������
 */
public class StartButtonListener implements ActionListener {

	private JPanel menuPanel;
	private GameFieldPanel fieldPanel;

	public StartButtonListener(JPanel menuPanel, GameFieldPanel fieldPanel) {
		super();
		this.menuPanel = menuPanel;
		this.fieldPanel = fieldPanel;
	}

	public void actionPerformed(ActionEvent e) {
		String port = null;
		String hostName = null;
		;
		String delay = null;
		fieldPanel.setFieldLocked(true);
		Component[] components = menuPanel.getComponents();
		ConnectionSettings settings = null;
		// ������ �� ����������� � ������� ����������
		for (Component component : components) {
			if (FieldDrawConstants.HOST_FIELD_NAME.equalsIgnoreCase(component
					.getName()) && component instanceof JTextField) {
				hostName = ((JTextField) component).getText();
			} else if (FieldDrawConstants.PORT_FIELD_NAME
					.equalsIgnoreCase(component.getName())
					&& component instanceof JTextField) {
				port = ((JTextField) component).getText();
			} else if (FieldDrawConstants.DELAY_FIELD_NAME
					.equalsIgnoreCase(component.getName())
					&& component instanceof JTextField) {
				delay = ((JTextField) component).getText();
			}
		}
		// �� ����, ������� ����������. ��������� ������� ConnectionSettings.
		// ��� �� � ������������ �������� ���������
		try {
			settings = new ConnectionSettings(hostName, port, delay);
		} catch (ConnectionSettingsException cse) {
			// ����� �� ����� � �����������
			JOptionPane.showMessageDialog(menuPanel, cse.getMessage(),
					"Config Error", JOptionPane.ERROR_MESSAGE);
			// ��������� �� ����������. ������� �����
			return;
		}
		// ��������� ������
		for (Component component : components) {
			// ��������� ������
			if ((FieldDrawConstants.START_BTN_NAME.equalsIgnoreCase(component
					.getName()) || FieldDrawConstants.RESET_BTN_NAME
					.equalsIgnoreCase(component.getName()))
					&& component instanceof JButton) {
				JButton button = (JButton) component;
				button.setEnabled(false);
			}
			if ((FieldDrawConstants.STOP_BTN_NAME.equalsIgnoreCase(component
					.getName()))
					&& component instanceof JButton) {
				JButton button = (JButton) component;
				button.setEnabled(true);
			}
		}

		GameFieldRefresher refresher = GameFieldRefresher.getInstance(settings,
				fieldPanel,menuPanel);
		refresher.start();

	}
}
