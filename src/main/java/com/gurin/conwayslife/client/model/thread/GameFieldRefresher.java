package com.gurin.conwayslife.client.model.thread;

import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.gurin.conwayslife.client.gui.FieldDrawConstants;
import com.gurin.conwayslife.client.gui.GameFieldPanel;
import com.gurin.conwayslife.client.model.GameField;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculationException;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculator;
import com.gurin.conwayslife.client.model.calc.impl.LifeLocalCalculator;
import com.gurin.conwayslife.client.model.calc.impl.LifeLocalNewCalculator;
import com.gurin.conwayslife.client.model.calc.impl.ServerLifeCalculator;
import com.gurin.conwayslife.client.model.calc.impl.StubCalculator;
import com.gurin.conwayslife.client.model.setting.ConnectionSettings;

public class GameFieldRefresher implements Runnable {
	// ���������� ��� �������� � �������
	private ConnectionSettings settings;
	// ������ ��� �����������
	private GameFieldPanel panel;

	private JPanel menuPanel;
	// ������ �� ������� ��������� � ���� ������
	private Thread thread;

	private boolean active;

	private static GameFieldRefresher refresher = null;

	private GameFieldCalculator calculator = null;

	private GameFieldRefresher() {
	}

	// ������������ ������������ ��������
	public static synchronized GameFieldRefresher getInstance(
			ConnectionSettings settings, GameFieldPanel panel, JPanel menuPanel) {
		if (refresher != null && refresher.settings.equals(settings)) {
			return refresher;
		} else if (refresher != null && !refresher.settings.equals(settings)) {
			refresher.setActive(false);
			refresher.stop();
			refresher = new GameFieldRefresher();
			refresher.panel = panel;
			refresher.menuPanel = menuPanel;
			refresher.settings = settings;
			refresher.calculator = new ServerLifeCalculator(settings);
			refresher.calculator.init();
			return refresher;
		} else {
			refresher = new GameFieldRefresher();
			refresher.panel = panel;
			refresher.menuPanel = menuPanel;
			refresher.settings = settings;
			refresher.calculator = new ServerLifeCalculator(settings);
			refresher.calculator.init();
			return refresher;
		}
	}

	public static synchronized void destroyCurrnentInstance() {
		if (refresher != null) {
			refresher.stop();
			refresher.thread = null;
			refresher.calculator.destroy();
			refresher.calculator = null;
			refresher = null;
		}
	}

	public void start() {
		setActive(true);
		calculator.init();
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		setActive(false);
		while (!thread.isInterrupted()) {
			thread.interrupt();
		}
		calculator.destroy();
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isActive() {
		return active;
	}

	@Override
	public void run() {
		System.out.println("Thread started");
		while (active) {
			GameField currentField = panel.getField();
			long startTime = System.currentTimeMillis();
			GameField newField = null;
			try {
				newField = calculator.calculate(currentField);
				panel.repaintGameField(newField);
			} catch (GameFieldCalculationException ex) {
				// ��� �� ��������.
				// ��������� �����
				// ������ ���, ����� �������� ����� Stop

				JOptionPane.showMessageDialog(panel, ex.getMessage(),
						"Runtime Exception", JOptionPane.ERROR_MESSAGE);
				ex.printStackTrace();
				Component[] comps = menuPanel.getComponents();
				for (Component component : comps) {
					if ((FieldDrawConstants.START_BTN_NAME
							.equalsIgnoreCase(component.getName()) && component instanceof JButton)
							|| FieldDrawConstants.RESET_BTN_NAME
									.equalsIgnoreCase(component.getName())
							&& component instanceof JButton) {
						((JButton) component).setEnabled(true);
					}
					if (FieldDrawConstants.STOP_BTN_NAME
							.equalsIgnoreCase(component.getName())
							&& component instanceof JButton) {
						((JButton) component).setEnabled(false);
					}
				}
				this.stop();
				panel.setFieldLocked(false);
				return;
			}
			long calcTime = System.currentTimeMillis() - startTime;
			// ��������, ������� ����������� ����.
			// �������� ������� ����� �� ����� t = �����_������� -
			// �����_���������;
			long timeToSleep = settings.getDelay() - calcTime >= 0 ? settings
					.getDelay() - calcTime : 0;
			try {
				Thread.sleep(timeToSleep);
			} catch (InterruptedException ex) {

			}

		}
	}
}
