package com.gurin.conwayslife.client.model.calc;

public class GameFieldCalculationException extends RuntimeException{

	public GameFieldCalculationException() {
		super();
	}

	public GameFieldCalculationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GameFieldCalculationException(String message, Throwable cause) {
		super(message, cause);
	}

	public GameFieldCalculationException(String message) {
		super(message);
	}

	public GameFieldCalculationException(Throwable cause) {
		super(cause);
	}
	
}
