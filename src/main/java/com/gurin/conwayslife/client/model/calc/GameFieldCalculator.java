package com.gurin.conwayslife.client.model.calc;

import com.gurin.conwayslife.client.model.GameField;

public interface GameFieldCalculator {
	public GameField calculate(GameField field) throws GameFieldCalculationException;
	
	public void init();
	
	public void destroy();
}
