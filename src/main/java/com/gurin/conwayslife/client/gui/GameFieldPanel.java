package com.gurin.conwayslife.client.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.gurin.conwayslife.client.gui.listener.FieldMouseClickListener;
import com.gurin.conwayslife.client.model.GameField;
import com.gurin.conwayslife.client.model.GameFieldFactory;

import static com.gurin.conwayslife.client.gui.FieldDrawConstants.CELL_SIZE;

public class GameFieldPanel extends JPanel {

	private volatile GameField field;
	
	private boolean fieldLocked = false;

	public GameFieldPanel() {
		resetField();
		setPreferredSize(new Dimension(field.getHeight() * CELL_SIZE,
				field.getHeight() * CELL_SIZE));
		setSize(getPreferredSize());
		addMouseListener(new FieldMouseClickListener(this));
	}

	public void repaintGameField(GameField field) {
		synchronized (field) {
			this.field = field;
		}
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int width = field.getWidth();
		int height = field.getHeight();
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (field.isCellAlive(i, j)) {
					g.fillRect(CELL_SIZE * i, CELL_SIZE * j, CELL_SIZE,
							CELL_SIZE);
				} else {
					g.drawRect(CELL_SIZE * i, CELL_SIZE * j, CELL_SIZE,
							CELL_SIZE);
				}
			}
		}
		// rectangle originated at 10,10 and end at 240,240

		// filled Rectangle with rounded corners.
	}

	public void resetField() {
		this.field = GameFieldFactory.generateEmptyField();
	}

	public GameField getField() {
			return field;
	}

	public boolean isFieldLocked() {
		return fieldLocked;
	}

	public void setFieldLocked(boolean fieldLocked) {
		this.fieldLocked = fieldLocked;
	}

}
