package com.gurin.conwayslife.client.model.setting;

public class ConnectionSettingsException extends Exception{

	public ConnectionSettingsException() {
		super();
	}

	public ConnectionSettingsException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ConnectionSettingsException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectionSettingsException(String message) {
		super(message);
	}

	public ConnectionSettingsException(Throwable cause) {
		super(cause);
	}
}
