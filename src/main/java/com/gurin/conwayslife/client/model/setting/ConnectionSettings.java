package com.gurin.conwayslife.client.model.setting;

public class ConnectionSettings {
	private Integer port;
	private Long delay;
	private String hostName;
	private static int PORT_MAX_VALUE = (int) Math.pow(2, 32);
	private static int PORT_MIN_VALUE = 1;

	private final String INVALID_HOST_MESSAGE = "Please, set host name";
	private final String INVALID_PORT_MESSAGE = "Port number should be an integer between 1 and 65565";
	private final String INVALID_DELAY_MESSAGE = "Delay should be and integer and > 0";

	public ConnectionSettings(String hostName, String portNumber,
			String delayNumber) throws ConnectionSettingsException {
		if (hostName == null || hostName.isEmpty()) {
			throw new ConnectionSettingsException(INVALID_HOST_MESSAGE);
		} else {
			this.hostName = hostName;
		}

		Integer port = null;
		try {
			port = Integer.parseInt(portNumber);
			if (port < PORT_MIN_VALUE || port > PORT_MAX_VALUE) {
				throw new ConnectionSettingsException(INVALID_PORT_MESSAGE);
			}
		} catch (NullPointerException | NumberFormatException ex) {
			throw new ConnectionSettingsException(INVALID_PORT_MESSAGE);
		}
		this.port = port;

		Long delay = null;
		try {
			delay = Long.parseLong(delayNumber);
			if (delay < 0) {
				throw new ConnectionSettingsException(INVALID_DELAY_MESSAGE);
			}
//		} catch (NullPointerException | NumberFormatException ex) {
		} catch (Exception ex) {
			throw new ConnectionSettingsException(INVALID_DELAY_MESSAGE);
		}
		this.delay = delay;
	}

	public Integer getPort() {
		return port;
	}

	public Long getDelay() {
		return delay;
	}

	public String getHostName() {
		return hostName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((delay == null) ? 0 : delay.hashCode());
		result = prime * result
				+ ((hostName == null) ? 0 : hostName.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ConnectionSettings other = (ConnectionSettings) obj;
		if (delay == null) {
			if (other.delay != null) {
				return false;
			}
		} else if (!delay.equals(other.delay)) {
			return false;
		}
		if (hostName == null) {
			if (other.hostName != null) {
				return false;
			}
		} else if (!hostName.equals(other.hostName)) {
			return false;
		}
		if (port == null) {
			if (other.port != null) {
				return false;
			}
		} else if (!port.equals(other.port)) {
			return false;
		}
		return true;
	}
	
	
}
