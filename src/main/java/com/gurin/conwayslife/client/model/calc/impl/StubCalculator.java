package com.gurin.conwayslife.client.model.calc.impl;

import com.gurin.conwayslife.client.model.GameField;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculationException;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculator;

public class StubCalculator implements GameFieldCalculator {
	private int iteration = 0;
	@Override
	public GameField calculate(GameField oldField)
			throws GameFieldCalculationException {
		if (++iteration == 100){
//			throw new GameFieldCalculationException("Oops");
		}
		int width = oldField.getWidth();
		int height = oldField.getHeight();
		double capaciy = 0.3; // ������� ��������
		
		GameField field = new GameField(width, height);
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if (Math.random() < capaciy) {
					field.addCell(i, j);
				}
			}
		}
		return field;
	}
	@Override
	public void init() {
	}
	@Override
	public void destroy() {
	}

}
