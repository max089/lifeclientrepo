package com.gurin.conwayslife.client.model.calc.impl;

import java.util.HashSet;
import java.util.Set;

import com.google.gson.Gson;
import com.gurin.conwayslife.client.model.Cell;
import com.gurin.conwayslife.client.model.GameField;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculationException;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculator;

public class LifeLocalNewCalculator implements GameFieldCalculator {

	@Override
	public GameField calculate(GameField field)
			throws GameFieldCalculationException {
		Gson gson = new Gson();
		System.out.println(gson.toJson(field));
		int width = field.getWidth();
		int heigt = field.getHeight();
		GameField newField = new GameField(width,heigt);
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < heigt; j++) {
				// � ������ (������) ������, ����� � ������� ����� ��� �����
				// ������, ����������� �����;
				int count = countAliveNeighboors(field,i,j);
				if (field.isCellDead(i, j)) {
					
					if (count == 3) {
						newField.addCell(i,j);
					}
				} else {
					// ���� � ����� ������ ���� ��� ��� ��� ����� �������, ��
					// ��� ������ ���������� ����; � ��������� ������ (����
					// ������� ������ ���� ��� ������ ���) ������ ������� (���
					// ����������� ��� ��� ���������������).
					if (count == 2 || count == 3){
						newField.addCell(i,j);
					} else {
						newField.removeCell(i,j);
					}
				}
			}
		}
		// ������ ���� ��������		
		return newField;
	}
    // ��������� ����� ������� � ������ ������
	private int countAliveNeighboors(GameField field,int i, int j) {
		return getCellVal(field, i - 1, j - 1)
				+ getCellVal(field, i, j - 1)
				+ getCellVal(field, i + 1, j - 1)
				+ getCellVal(field, i - 1, j)
				+ getCellVal(field, i + 1, j)
				+ getCellVal(field, i - 1, j + 1)
				+ getCellVal(field, i, j + 1)
				+ getCellVal(field, i + 1, j + 1);
	}
	
	// ������� ����� ������, ����� �� ������� � ���������
	private int getCellVal(GameField field, int x, int y) {
		return field.isCellAlive(x, y) ? 1 : 0; 
	}
	@Override
	public void init() {
		
	}
	@Override
	public void destroy() {
		
	}
	
}
