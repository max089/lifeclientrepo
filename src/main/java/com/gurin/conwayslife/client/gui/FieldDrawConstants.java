package com.gurin.conwayslife.client.gui;

public abstract class FieldDrawConstants {
	private FieldDrawConstants(){};
	public static final int CELL_SIZE = 10;
	public static final String START_BTN_NAME = "start_button";
	public static final String STOP_BTN_NAME = "stop_button";
	public static final String RESET_BTN_NAME = "reset_button";
	
	public static final String PORT_FIELD_NAME = "port_field";
	public static final String HOST_FIELD_NAME = "host_field";
	public static final String DELAY_FIELD_NAME = "delay_field";
}
