package com.gurin.conwayslife.client.gui.listener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import com.gurin.conwayslife.client.gui.GameFieldPanel;
import com.gurin.conwayslife.client.model.GameField;
import static com.gurin.conwayslife.client.gui.FieldDrawConstants.CELL_SIZE;

public class FieldMouseClickListener implements MouseListener {
	private GameFieldPanel panel;

	public FieldMouseClickListener(GameFieldPanel panel) {
		super();
		this.panel = panel;
	}

	public void mouseClicked(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		GameField field = panel.getField();
		if (!panel.isFieldLocked()) {
			if (SwingUtilities.isLeftMouseButton(e)) {
				field.addCell(x / CELL_SIZE, y / CELL_SIZE);
				panel.repaintGameField(field);
			} else if (SwingUtilities.isRightMouseButton(e)) {
				field.removeCell(x / CELL_SIZE, y / CELL_SIZE);
				panel.repaintGameField(field);
			}
		} else {
			System.out.println("field is lockaed");
		}
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}

	public void mousePressed(MouseEvent e) {
	}

	public void mouseReleased(MouseEvent e) {
	}

}
