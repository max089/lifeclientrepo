package com.gurin.conwayslife.client.gui.listener;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import com.gurin.conwayslife.client.gui.FieldDrawConstants;
import com.gurin.conwayslife.client.gui.GameFieldPanel;
import com.gurin.conwayslife.client.model.thread.GameFieldRefresher;
/*
 * ��� ��� ���� �������:
 * 1. ���������� �����
 * 2. ��������� ������
 * 3. ��������� ����
 */
public class StopButtonListener implements ActionListener {
	private JPanel menuPanel;
	private final GameFieldPanel fieldPanel;

	public StopButtonListener(JPanel menuPanel, GameFieldPanel fieldPanel) {
		super();
		this.menuPanel = menuPanel;
		this.fieldPanel = fieldPanel;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// �������� �����
		GameFieldRefresher.destroyCurrnentInstance();
		// ��������� ������
		fieldPanel.setFieldLocked(false);
		Component[] components = menuPanel.getComponents();
		for (Component component : components) {
			// �������� ������
			if ((FieldDrawConstants.START_BTN_NAME.equalsIgnoreCase(component
					.getName()) || FieldDrawConstants.RESET_BTN_NAME
					.equalsIgnoreCase(component.getName()))
					&& component instanceof JButton) {
				JButton button = (JButton) component;
				button.setEnabled(true);
			}
		}
		((JButton)event.getSource()).setEnabled(false);
		
	}
	
	
}
