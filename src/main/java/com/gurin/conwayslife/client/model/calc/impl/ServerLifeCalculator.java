package com.gurin.conwayslife.client.model.calc.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;
import com.gurin.conwayslife.client.model.GameField;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculationException;
import com.gurin.conwayslife.client.model.calc.GameFieldCalculator;
import com.gurin.conwayslife.client.model.setting.ConnectionSettings;

public class ServerLifeCalculator implements GameFieldCalculator {
	private ConnectionSettings settings;

	private HttpClient client;
	private HttpPost post = new HttpPost();
	private Gson gson = new Gson();
	private HttpHost host;

	public ServerLifeCalculator(ConnectionSettings settings) {
		this.settings = settings;
		HttpClientBuilder builder = HttpClientBuilder.create();
		builder.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy());
		client = builder.build();
	}

	@Override
	public GameField calculate(GameField field)
			throws GameFieldCalculationException {
		try {
			post = new HttpPost(settings.getHostName());
			String json = gson.toJson(field);
			if (json != null) {
				post.addHeader(HttpHeaders.CONTENT_TYPE, "application/json");
				HttpEntity entity = new StringEntity(json);
				post.setEntity(entity);
				HttpResponse res = client.execute(host, post);
				HttpEntity responseEntity = res.getEntity();
				BufferedReader reader = new BufferedReader(new InputStreamReader(responseEntity.getContent())); 
				GameField newField = gson.fromJson(reader, GameField.class);
				
				return newField;
			} else {
				throw new IllegalArgumentException("Serealized json is null!!");
			}
		} catch (Exception ex) {
			throw new GameFieldCalculationException(ex);
		} finally {
			
		}
	}

	@Override
	public void init() {
		host = new HttpHost(settings.getHostName(),settings.getPort());
		System.out.println("Port is "+settings.getPort());
	}

	@Override
	public void destroy() {
		
	}
}
