package com.gurin.conwayslife.client.model;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class GameFieldFactory {
	public static GameField generateGlider(){
		Set<Cell> cells = new HashSet<Cell>(){
			{
				add(new Cell(2, 1));
				add(new Cell(1, 2));
				add(new Cell(1, 3));
				add(new Cell(2, 3));
				add(new Cell(3, 3));
			}
		};
		GameField field = new GameField(100, 100, cells, false);
		return field;
	}
	
	public static GameField generateEmptyField(){
		GameField field = new GameField(80, 80, new HashSet<Cell>(), false);
		return field;
	}
}
