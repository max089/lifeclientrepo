package com.gurin.conwayslife.client.gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.gurin.conwayslife.client.gui.GameFieldPanel;
import com.gurin.conwayslife.client.model.GameFieldFactory;

public class ResetButtonListener implements ActionListener{
	GameFieldPanel panel;
	
	
	public ResetButtonListener(GameFieldPanel panel) {
		super();
		this.panel = panel;
	}

	public void actionPerformed(ActionEvent e) {
		panel.resetField();
		panel.setFieldLocked(false);
		panel.repaint();
//		panel.repaintGameField(field)();
	}
	
}
