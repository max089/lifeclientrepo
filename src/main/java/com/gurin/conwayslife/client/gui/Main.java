package com.gurin.conwayslife.client.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.gurin.conwayslife.client.gui.listener.ResetButtonListener;
import com.gurin.conwayslife.client.gui.listener.StartButtonListener;
import com.gurin.conwayslife.client.gui.listener.StopButtonListener;
import com.gurin.conwayslife.client.model.GameFieldFactory;

public class Main extends JFrame {
	GameFieldPanel jp;
	JButton startButton = new JButton("Start");
	JButton stopButton = new JButton("Stop");
	JButton resetButton = new JButton("Reset");

	JLabel portLabel = new JLabel("Port:");
	JLabel hostLabel = new JLabel("Host:");
	JLabel delayLabel = new JLabel("Delay:");
	
	
	JTextField hostTextField = new JTextField("localhost",15);
	JTextField portTextField = new JTextField("8080",6);
	JTextField delayTextField = new JTextField("1000",3);

	public Main() {
		super("Conway's Life");
		organizeComponents();
	}

	public void organizeComponents() {
		JPanel menu = new JPanel();
		jp = new GameFieldPanel();
		hostLabel.setSize(startButton.getSize());

		FlowLayout l = new FlowLayout();
//		l.setHgap(50);
		menu.setLayout(l);
		
		// ������
		startButton.setName(FieldDrawConstants.START_BTN_NAME);
		startButton.addActionListener(new StartButtonListener(menu,jp));
		menu.add(startButton);
		
		stopButton.setName(FieldDrawConstants.STOP_BTN_NAME);
		stopButton.addActionListener(new StopButtonListener(menu, jp));
		stopButton.setEnabled(false);
		menu.add(stopButton);
		
		resetButton.setName(FieldDrawConstants.RESET_BTN_NAME);
		resetButton.addActionListener(new ResetButtonListener(jp));
		menu.add(resetButton);
		
		menu.add(hostLabel);
		hostTextField.setName(FieldDrawConstants.HOST_FIELD_NAME);
		menu.add(hostTextField);
		
		menu.add(portLabel);
		portTextField.setName(FieldDrawConstants.PORT_FIELD_NAME);
		menu.add(portTextField);
		
		menu.add(delayLabel);
		delayTextField.setName(FieldDrawConstants.DELAY_FIELD_NAME);
		menu.add(delayTextField);
		
		//������
		setResizable(false);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		setLayout(new BorderLayout());
		add(menu, BorderLayout.BEFORE_FIRST_LINE);
		// add(stopButton,BorderLayout.LINE_END);
		
		add(jp, BorderLayout.CENTER);

		Dimension dimension = (Dimension) jp.getSize();
		pack();
		dimension.setSize(dimension.getWidth() + getInsets().left
				+ getInsets().right + 1, dimension.getHeight()
				+ getInsets().bottom + getInsets().top + menu.getHeight() + 1);
		setSize(dimension);
	}

	public static void main(String[] args) {
		Main g1 = new Main();
		g1.setVisible(true);
		System.out.println("Done bitch");
	}
}